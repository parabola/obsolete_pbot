


(ignore-errors
  (defvar fs-after-load-hooks 'nil))



(ignore-errors
  (defvar fs-before-load-hooks 'nil))



(ignore-errors
  (defvar fs-bunny '142857))



(ignore-errors
  (defvar fs-dunnet-mode 'nil))



(ignore-errors
  (defvar fs-e '2.718281828459045))



(ignore-errors
  (defvar fs-emacs-version '"23.3.1"))



(ignore-errors
  (defvar fs-euler '2.718281828459045))



(ignore-errors
  (defvar fs-flame-target 'nil))



(ignore-errors
  (defvar fs-found-query-p 'nil))



(ignore-errors
  (defvar fs-google-level '60))



(ignore-errors
  (defvar fs-home-page '"http://www.emacswiki.org/cgi-bin/wiki.pl?ErBot"))



(ignore-errors
  (defvar fs-internal-add-nick-weights
    '(1 5)))



(ignore-errors
  (defvar fs-internal-addressedatlast 'nil))



(ignore-errors
  (defvar fs-internal-articles
    '("the" "a" "an" "this" "that")))



(ignore-errors
  (defvar fs-internal-botito-mode 'nil))



(ignore-errors
  (defvar fs-internal-botread-prompt '"Enter: "))



(ignore-errors
  (defvar fs-internal-describe-literally-p 'nil))



(ignore-errors
  (defvar fs-internal-dictionary-time '4))



(ignore-errors
  (defvar fs-internal-directed 'nil))



(ignore-errors
  (defvar fs-internal-doctor-rarity '80))



(ignore-errors
  (defvar fs-internal-english-max-matches '20))



(ignore-errors
  (defvar fs-internal-english-target-regexp '"^$"))



(ignore-errors
  (defvar fs-internal-english-weights
    '(30 30 30 2)))



(ignore-errors
  (defvar fs-internal-fill-column '350))



(ignore-errors
  (defvar fs-internal-google-level '60))



(ignore-errors
  (defvar fs-internal-google-redirect-p 'nil))



(ignore-errors
  (defvar fs-internal-google-time '4))



(ignore-errors
  (defvar fs-internal-h4x0r-maybe-weights
    '(100 1)))



(ignore-errors
  (defvar fs-internal-limit-length '300))



(ignore-errors
  (defvar fs-internal-limit-line-length '125))



(ignore-errors
  (defvar fs-internal-max-lisp-p 'nil))



(ignore-errors
  (defvar fs-internal-message-sans-bot-name '""))



(ignore-errors
  (defvar fs-internal-original-message '"yeah. because when a git repo is initiated it has 700 permissions"))



(ignore-errors
  (defvar fs-internal-parse-error-p 't))



(ignore-errors
  (defvar fs-internal-parse-preprocess-message-remove-end-chars
    '(1)))



(ignore-errors
  (defvar fs-internal-query-target-regexp '""))



(ignore-errors
  (defvar fs-internal-questions
    '("what" "where" "who")))



(ignore-errors
  (defvar fs-internal-questions-all
    '("what" "where" "who" "why" "how" "whose" "which")))



(ignore-errors
  (defvar fs-internal-studlify-maybe-weights
    '(100 1)))



(ignore-errors
  (defvar fs-limit-lines '8))



(ignore-errors
  (defvar fs-lispa 'nil))



(ignore-errors
  (defvar fs-lispargs 'nil))



(ignore-errors
  (defvar fs-lispb 'nil))



(ignore-errors
  (defvar fs-lispc 'nil))



(ignore-errors
  (defvar fs-lispd 'nil))



(ignore-errors
  (defvar fs-lispe 'nil))



(ignore-errors
  (defvar fs-msg '"The exact current message being parsed. "))



(ignore-errors
  (defvar fs-msglist '"Message broken into list.  This list may have\nremoved characters like ?  and ,,  No guarantees here.  See\nfs-msgsandbot instead."))



(ignore-errors
  (defvar fs-msglistsansbot 'nil))



(ignore-errors
  (defvar fs-msgsansbot 'nil))



(ignore-errors
  (defvar fs-nick '""))



(ignore-errors
  (defvar fs-nil 'nil))



(ignore-errors
  (defvar fs-pi '3.141592653589793))



(ignore-errors
  (defvar fs-pv-save-rarity '100000))



(ignore-errors
  (defvar fs-set-add-all-p 'nil))



(ignore-errors
  (defvar fs-t 't))



(ignore-errors
  (defvar fs-tgt 'nil))



(ignore-errors
  (defvar fs-version '"0.0dev"))



(ignore-errors
  (defvar fs-web-page-title-p 't))



